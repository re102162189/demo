const async = require('async');
const express = require('express');
const session = require('express-session');
const router = express.Router();
router.use(session({
  secret: '123456789',
  resave: true,
  saveUninitialized: true
}));

const apiKey = 'k3MDLDLam8QJnPSlk4tX1Od_Wy2DxCfXEPkA52werKNl';
const url = 'https://gateway-tok.watsonplatform.net/assistant/api';
const assistant_id = 'f796e278-a7cf-4380-847b-4287984b6894';

const AssistantV2 = require('ibm-watson/assistant/v2');
const service = new AssistantV2({
  iam_apikey: apiKey,
  version: '2019-02-28',
  url: url
});

const sessionMap = [];
const close_token = 'thanks';

router.get('/order', function (req, res) {
  async.waterfall(
    [
      async.apply(createSession, req, res),
      message,
      deleteSession,
    ]
    , (err) => {
      console.log(err);
    });
});

function createSession(req, res, next) {
  if (req.session.id in sessionMap) {
    next(null, req, res);
  } else {
    service
      .createSession({
        assistant_id: assistant_id
      })
      .then(response => {
        sessionMap[req.session.id] = response.session_id;
        next(null, req, res);
      })
      .catch(err => {
        next(err);
      });
  }
}

function message(req, res, next) {
  service
    .message({
      assistant_id: assistant_id,
      session_id: sessionMap[req.session.id],
      input: {
        'message_type': 'text',
        'text': req.query.text
      }
    })
    .then(response => {
      res.setHeader('Content-Type', 'application/json');
      res.send(JSON.stringify(response, null, 2));

      var output = response['output'];
      if ('intents' in output) {
        var intents = output['intents'];
        intents.forEach(function (intent) {
          if (intent['intent'].toLowerCase() == close_token) {
            console.log(intent);
            next(null, req);
          }
        });
      }
    })
    .catch(err => {
      next(err);
    });
};

function deleteSession(req, next) {
  service
    .deleteSession({
      assistant_id: assistant_id,
      session_id: sessionMap[req.session.id],
    })
    .then(res => {
      delete sessionMap[req.session.id];
      next(null);
    })
    .catch(err => {
      next(err);
    });
}

module.exports = router;